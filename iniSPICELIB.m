%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializes Paths for SPICELIB within some project
%
% Description:
%  This script finds location of SPICELIB root and adds the required paths
%  to the main Matlab paths.
%
% Modifications:
%  MA-14Jun16 initial draft
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SPICELIBrootdir = fileparts( mfilename('fullpath'));

% Set the path of the version of MICE to use based on computer arch type
archstr= computer('arch');
if strcmp(archstr,'win64')
  MICEPATH = '002-MICE_win64';
elseif strcmp(archstr,'glnxa64')
  MICEPATH = '003-MICE_linux64';
elseif strcmp(archstr,'maci64')
  MICEPATH = '004-MICE_mac64';
else
  error(['No Valid version of MICE found! Visit NAIF to see if a version exists for ', archstr, ' architecture add it, and update this script to use it'])
end

%% Add paths to required spice parts and minimum kernel sets.
addpath(fullfile(SPICELIBrootdir,'001-MinimumKernels'))
addpath(fullfile(SPICELIBrootdir, MICEPATH, 'src/mice'))
addpath(fullfile(SPICELIBrootdir, MICEPATH, 'lib'))
% SPICEPATHstr= which('mice');
clear SPICELIBrootdir MICEPATH archstr